<?php
namespace Wolfhowlmedia;

/**
 * The class for colorizing shell output
 *
 * @example
 * echo ColorConsole::bgred("Ninjas", ['fg'=>'yellow', 'mut'=>'rev'])->clear(" ")->green("are cool.")->show(true);
 */
class Colorify {
	//Neutral FG color
	const FGCLEAR   = '0';

	//ANSI colors (FG)
	const FGBLACK   = '30';
	const FGRED     = '31';
	const FGGREEN   = '32';
	const FGYELLOW  = '33';
	const FGBLUE    = '34';
	const FGMAGENTA = '35';
	const FGCYAN    = '36';
	const FGWHITE   = '37';

	//ANSI colors (BG)
	const BGBLACK   = '40';
	const BGRED     = '41';
	const BGGREEN   = '42';
	const BGYELLOW  = '43';
	const BGBLUE    = '44';
	const BGMAGENTA = '45';
	const BGCYAN    = '46';
	const BGWHITE   = '47';

	//permutations
	const PERMNORM  = 0; //normal
	const PERMLIGHT = 1; //light
	const PERMDARK  = 2; //dark
	const PERMUNDER = 4; //underline
	const PERMBLINK = 5; //blink
	const PERMREV   = 7; //reverse
	const PERMHIDE  = 8; //hide

	private $str = '';

	/**
	 * Interpret the static call
	 * @param  $name      name of the color
	 * @param  $arguments Additional arguments (fg color, bg color, mutator)
	 * @return            self-object
	 */
	public static function __callStatic( $name, $arguments ) {
		$out = self::parse($name, $arguments);
		$obj = new self;
		$obj->str = $out;
		return $obj;
	}

	/**
	 * Do exactly like the __callStatic does, except do so with objects rather than static calls
	 * @param   $func   Name of the color
	 * @param   $params Additional arguments (fg color, bg color, mutator)
	 * @return          self-object
	 */
	public function __call($func, $params){
		$this->str .= self::parse($func, $params);
		return $this;
	}

	/**
	 * Return indent character (chr 7)
	 */
	public static function indent() {
		return " • ";
	}

	/**
	 * Creates a line of specified length (defaults to _ of 5 characters) with optional markers (e.g. +)
	 * @param  integer $len            Lenght of the line. Defaults to 5
	 * @param  string  $char           Line character. Defaults to _
	 * @param  boolean $append_markers Append markers (such as +). Defaults to false
	 * @param  string  $markers        Which marker to append (beginning and end). Defaults to +
	 * @return string                  Output the line string
	 */
	public static function line($len = 5, $char = "=", $append_markers = false, $markers = "+") {
		$out = '';
		$out .= $append_markers ? $markers : '';
		$out .= str_pad('', $len, $char);
		$out .= $append_markers ? $markers : '';
		return $out;
	}


	/**
	 * Return the formatted string.
	 * @param  boolean $nl Make new line (default = false)
	 * @return self-object
	 */
	public function show($nl = false) {
		return $this->str . ($nl ? "\n" : '');
	}

	/**
	 * Parse function names and arguments
	 * @param  string $name     Name of the color (default fg if the prefix is omitted)
	 * @param  array $arguments array of key / value arguments ['fg', 'bg', 'mut']
	 * @return string           formatted string
	 */
	private static function parse($name, $arguments) {
		if (!isset($arguments[0])) {
			$arguments[0] = '';
		}
		$args = isset($arguments[1]) ? $arguments[1] : [];
		$out = self::color_decoder($name, $args);
		foreach(['fg', 'bg'] as $gr ){
			if (isset($args[$gr])) {
				$tmp = strtoupper($gr)."{$args[$gr]}";
				$out .= self::color_decoder($tmp, $args);
			}
		}

		$out .=  $arguments[0] . "\033[0m";

		return $out;
	}

	/**
	 * Decode the name of the function and turn it into a proper color, called by the parser function
	 * @param  string $name Name of the color (default fg if the prefix is omitted)
	 * @param  array  $args array of key / value arguments ['fg', 'bg', 'mut']
	 * @return string       parsed text
	 */
	private static function color_decoder($name = 'clear', $args = []) {
		$out = '';
		$tmp = strtoupper($name);
		$mut = isset($args['mut']) ? strtoupper('PERM'.$args['mut']) : '';
		if ($mut && defined("self::$mut")) {
			$mut = "\033[" . constant('self::'. $mut) . "m";
		}

		if (defined("self::$tmp")) {
			$out = $mut."\033[" . constant('self::'. $tmp) . "m";
		} else if (defined("self::FG$tmp")) {
			$tmp = 'FG'.$tmp;
			$out = $mut."\033[" . constant('self::'. $tmp) . "m";
		}

		return $out;
	}
}
