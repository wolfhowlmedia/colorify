Colorify
=============

Make colorful console output with PHP

#Installation

 `composer require wolfhowlmedia/colorify '~1.0'`

#How to use

 ```php
 <?php

 require_once '../vendor/autoload.php';

 use Wolfhowlmedia\Colorify;

 echo Colorify::red("ninjas?")->show(true);

 echo Colorify::green(Colorify::ident())->show(true);
 
 echo Colorify::green(Colorify::line(15, '=-', false))->show(true);

 ```

Colors (and BGs): 
 - BLACK
 - RED
 - GREEN
 - YELLOW
 - BLUE
 - MAGENTA
 - CYAN
 - WHITE

Mutators:
 - NORM
 - LIGHT
 - DARK
 - dependant on the terminal:
   - UNDER
   - BLINK
   - REV
   - HIDE

```php
 echo Colorify::clear(" ")->blue("ninjas?", ['mut' => 'light'])->show(true);
```


