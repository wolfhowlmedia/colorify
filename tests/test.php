<?php
require_once '../src/Colorify/Colorify.php';

echo Wolfhowlmedia\Colorify::clear(Wolfhowlmedia\Colorify::indent())->red("This", ['bg'=>'blue', 'mut'=>'light'])->clear(" ")->green(' is ')->yellow(' sparta! ', ['mut' => 'light'])->show(true);
echo Wolfhowlmedia\Colorify::green(Wolfhowlmedia\Colorify::line(15, '=-', false, '•'))->show(true);
